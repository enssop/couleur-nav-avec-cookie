<?php

if ( isset($_GET['color']) && in_array($_GET['color'], ['orange', 'blue', 'green']) ) {

    setcookie("color", $_GET['color']);
    header('Location: index.php');

}

?><!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>test avec les cookies</title>
    <style>
        body {
            margin-top: 50px;
        }

        nav {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
        }

        .orange {
            background-color: orange;
        }

        .blue {
            background-color: blue;
        }

        .green {
            background-color: green;
        }
    </style>
</head>
<body>
    
    <nav class="<?php echo ( isset($_COOKIE['color']) && in_array($_COOKIE['color'], ['orange', 'blue', 'green'])) ? $_COOKIE['color'] : 'orange'; ?>">
        une navbar
    </nav>

<a href="/index.php?color=orange">orange</a> - 
<a href="/index.php?color=blue">bleu</a> - 
<a href="/index.php?color=green">vert</a>

</body>
</html>